# BackendChallengeGabrielCoelho

**API de um produto de controle de candidatos para vagas de empregos**



**Funcionalidades:**

* [ ]  Deve ser possível cadastrar usuários que vão administrar o sistema

**Os Administradores Podem**


* [ ]  Criar vagas de emprego

* [ ]  Cadastrar Candidatos

* [ ]  Relacionar uma ou mais vagas a um candidato 

* [ ]  Adicionar comentários a cada candidatura

**Requisitos Não Funcionais**

*  Utilizar banco de dados relacional

*  Utilizar ultima versão LTS do Node.js

*  Utilizar ultima versão LTS do Node.js

*  Efetuar testes